<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Haskell" FOLDED="false" ID="ID_1476109000" CREATED="1522483729261" MODIFIED="1522483733018" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Tipos primitivos" POSITION="right" ID="ID_343335136" CREATED="1522483734937" MODIFIED="1522483746219">
<node TEXT="Ponto flutuante" ID="ID_1592694940" CREATED="1522483747470" MODIFIED="1522483753140"/>
<node TEXT="Inteiro" ID="ID_1871071097" CREATED="1522483753429" MODIFIED="1522483755772"/>
<node TEXT="Caracteres" ID="ID_1429579843" CREATED="1522483805092" MODIFIED="1522483807541"/>
<node TEXT="Lista" ID="ID_676815654" CREATED="1522483756091" MODIFIED="1522483768351">
<node TEXT="De Tipo &#xfa;nico" ID="ID_597716599" CREATED="1522483796572" MODIFIED="1522483803999"/>
<node TEXT="Strings s&#xe3;o listas de caracteres" ID="ID_789374801" CREATED="1522483810211" MODIFIED="1522483833096"/>
</node>
<node TEXT="Fun&#xe7;&#xe3;o" ID="ID_1645170862" CREATED="1522483758340" MODIFIED="1522483765591">
<node TEXT="N&#xe3;o existem vari&#xe1;veis que n&#xe3;o sejam par&#xe2;metros ou retornos" ID="ID_1870041487" CREATED="1522483770364" MODIFIED="1522483791089"/>
</node>
<node TEXT="tuples" ID="ID_752169492" CREATED="1522484362851" MODIFIED="1522484364790"/>
</node>
<node TEXT="Algumas Fun&#xe7;&#xf5;es interessantes" POSITION="left" ID="ID_1308499373" CREATED="1522483840366" MODIFIED="1522483847085">
<node TEXT="succ" ID="ID_153874880" CREATED="1522484027081" MODIFIED="1522484029600"/>
<node TEXT="max" ID="ID_250897150" CREATED="1522484030299" MODIFIED="1522484033036"/>
<node TEXT="min" ID="ID_692105908" CREATED="1522484034345" MODIFIED="1522484035756"/>
<node TEXT="Fun&#xe7;&#xf5;es com par&#xe2;metros de lista" ID="ID_222843028" CREATED="1522484037384" MODIFIED="1522484048667">
<node TEXT="head" ID="ID_1182383812" CREATED="1522484229579" MODIFIED="1522484231111"/>
<node TEXT="tail" ID="ID_1879737684" CREATED="1522484231483" MODIFIED="1522484238245"/>
<node TEXT="last" ID="ID_1735245291" CREATED="1522484238634" MODIFIED="1522484241237"/>
<node TEXT="init" ID="ID_1821107159" CREATED="1522484241771" MODIFIED="1522484243160"/>
<node TEXT="length" ID="ID_1582668918" CREATED="1522484246561" MODIFIED="1522484249555"/>
<node TEXT="null" ID="ID_1289814883" CREATED="1522484250672" MODIFIED="1522484252274"/>
<node TEXT="reverse" ID="ID_1434280334" CREATED="1522484256160" MODIFIED="1522484258041"/>
<node TEXT="take" ID="ID_1466104417" CREATED="1522484258576" MODIFIED="1522484259529"/>
<node TEXT="drop" ID="ID_270524092" CREATED="1522484263078" MODIFIED="1522484264124"/>
<node TEXT="maximum" ID="ID_1978805471" CREATED="1522484264620" MODIFIED="1522484267429"/>
<node TEXT="minimum" ID="ID_1566872927" CREATED="1522484267868" MODIFIED="1522484270145"/>
<node TEXT="sum" ID="ID_1524630231" CREATED="1522484270948" MODIFIED="1522484275705"/>
<node TEXT="product" ID="ID_292308210" CREATED="1522484276290" MODIFIED="1522484278103"/>
<node TEXT="elem" ID="ID_643793299" CREATED="1522484281449" MODIFIED="1522484285250"/>
<node TEXT="cycle" ID="ID_1061419673" CREATED="1522484298753" MODIFIED="1522484300462"/>
<node TEXT="repeat" ID="ID_1410864112" CREATED="1522484301632" MODIFIED="1522484304248"/>
<node TEXT="replicate" ID="ID_127925357" CREATED="1522484314908" MODIFIED="1522484317986"/>
</node>
</node>
<node TEXT="List comprehension" POSITION="right" ID="ID_633869713" CREATED="1522483851296" MODIFIED="1522483976569">
<node TEXT="Defini&#xe7;&#xe3;o de lista parecida com defini&#xe7;&#xe3;o de conjunto na matem&#xe1;tica" ID="ID_1623490005" CREATED="1522483860209" MODIFIED="1522483874865">
<node TEXT="\latex lista = [x*2 | x\leftarrow[1..10] ]" ID="ID_804369385" CREATED="1522483876706" MODIFIED="1522483951860"/>
</node>
</node>
<node TEXT="operadores" POSITION="left" ID="ID_1976010358" CREATED="1522484071759" MODIFIED="1522484074864">
<node TEXT="l&#xf3;gicos" ID="ID_92542887" CREATED="1522484075956" MODIFIED="1522484079184">
<node TEXT="&amp;&amp;" ID="ID_902809445" CREATED="1522484109810" MODIFIED="1522484111713"/>
<node TEXT="||" ID="ID_1627685725" CREATED="1522484112802" MODIFIED="1522484114073"/>
<node TEXT="==" ID="ID_1920593061" CREATED="1522484117281" MODIFIED="1522484119161"/>
<node TEXT="/=" ID="ID_16170593" CREATED="1522484120888" MODIFIED="1522484123648"/>
<node TEXT="not" ID="ID_1680964247" CREATED="1522484124625" MODIFIED="1522484130910"/>
</node>
<node TEXT="aritm&#xe9;ticos" ID="ID_1581075932" CREATED="1522484080547" MODIFIED="1522484084214">
<node TEXT="+" ID="ID_1054832676" CREATED="1522484085995" MODIFIED="1522484087256"/>
<node TEXT="-" ID="ID_91089840" CREATED="1522484087878" MODIFIED="1522484088432"/>
<node TEXT="/" ID="ID_1574934871" CREATED="1522484089088" MODIFIED="1522484091236"/>
<node TEXT="*" ID="ID_1439485895" CREATED="1522484091770" MODIFIED="1522484094104"/>
<node TEXT="mod" ID="ID_436354270" CREATED="1522484331558" MODIFIED="1522484332703"/>
</node>
<node TEXT="para listas" ID="ID_536002617" CREATED="1522484140593" MODIFIED="1522484145333">
<node TEXT="++" ID="ID_362691378" CREATED="1522484147091" MODIFIED="1522484149404">
<node TEXT="Concatena&#xe7;&#xe3;o de listas" ID="ID_909176887" CREATED="1522484150344" MODIFIED="1522484158685"/>
</node>
<node TEXT=":" ID="ID_654666797" CREATED="1522484162265" MODIFIED="1522484165112">
<node TEXT="Empilha elemento na lista (sempre do lado esquerdo)" ID="ID_1280898255" CREATED="1522484166935" MODIFIED="1522484182624"/>
</node>
</node>
</node>
</node>
</map>
